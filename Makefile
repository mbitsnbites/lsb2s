# -*- mode: Makefile; tab-width: 8; indent-tabs-mode: t; -*-
#-----------------------------------------------------------------------------
# Copyright (c) 2024 Marcus Geelnard
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#-----------------------------------------------------------------------------

CFLAGS ?= -O3
CFLAGS += -W -Wall -Wextra
LDFLAGS ?= -s

ifeq ($(PREFIX),)
PREFIX := /usr/local
endif

OUT ?= out

.PHONY: all clean install uninstall

all: $(OUT)/lsb2s

clean:
	rm -f $(OUT)/lsb2s

install: $(OUT)/lsb2s
	install -d "$(DESTDIR)$(PREFIX)/bin"
	install -m 755 "$(OUT)/lsb2s" "$(DESTDIR)$(PREFIX)/bin"

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/lsb2s"

$(OUT):
	mkdir -p $(OUT)

$(OUT)/lsb2s: lsb2s.c $(OUT)
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

