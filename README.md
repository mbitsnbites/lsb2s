# lsb2s - Ludicrous speed binary to source converter

Convert binary data to source code, at [ludicrous speed](https://youtu.be/Rek9KYnqQnU?t=51) (because light speed is too slow)!

Supported languages:

* C
* Go
* JavaScript

## Installation

```bash
make
sudo make install
```

## Usage

Convert `binaryfile.raw` to `binaryfile.c`:

```bash
lsb2s -n mydata -o binaryfile.c binaryfile.raw
```

The contents of `binaryfile.c` would look something like this:

```C
const unsigned char mydata[] = ""
 "\x49\x63\xfe\xbc\x14\x8a\x25\x59\x3a\x21\xc3\x75\x24\xe0\xb8\x1d\xff\xa1\xa9\xc0\x4b\x54\x18\x26\xdf\xb2\xf4\x25\xe7\x2a\xc5\xf4"
 "\x5f\xcb\x7a\xe0\xc1\x37\xdf\xf7\x25\x81\x63\x1a\xb4\x0f\xa3\x8c\xce\xb5\xf5\x97\x49\xf4\x24\xa2\x5c\x50\xee\x4c\x27\x1c\xef\xa5"
 ...
 "\x21\xfd\xcb\x93\x52\x8e\x81\xf9\x38\x23\xd4\x1b\x34\x94\x95\x8c\xae\x16\xa9\x49\x3b\x9a\xdb\x69\xcc\xaa\xd2\x60\x6b\x87\x96\x38"
 "\xbc\x22\x32\x33\x63\x89\xd8\xaf\xf7\xae\x0e\xce\xb2\x21\x0f\x5c\x6f\xe7";
const unsigned int mydata_len = 1234U;

```

By default, lsb2s generates string literals. To get an array of numeric literals instead, pass the `--numeric` flag, which gives:

```C
const unsigned char mydata[] = {
    73, 99,254,188, 20,138, 37, 89, 58, 33,195,117, 36,224,184, 29,255,161,169,192, 75, 84, 24, 38,223,178,244, 37,231, 42,197,244,
    95,203,122,224,193, 55,223,247, 37,129, 99, 26,180, 15,163,140,206,181,245,151, 73,244, 36,162, 92, 80,238, 76, 39, 28,239,165,
   ...
    33,253,203,147, 82,142,129,249, 56, 35,212, 27, 52,148,149,140,174, 22,169, 73, 59,154,219,105,204,170,210, 96,107,135,150, 56,
   188, 34, 50, 51, 99,137,216,175,247,174, 14,206,178, 33, 15, 92,111,231,};
const unsigned int mydata_len = 1234U;
```

If you want prettier output, pipe the result through a code formatter such as [clang-format](https://clang.llvm.org/docs/ClangFormat.html):

```bash
lsb2s -n mydata --numeric binaryfile.raw | clang-format > binaryfile.c
```

Convert an image to raw pixels using [ImageMagick](https://imagemagick.org/):

```bash
stream -map rgb -storage-type char spaceballs.jpg /dev/stdout | lsb2s -n spaceballs > spaceballs.c
```

Invoke lsb2s with the `--help` option for more details.

## String literals vs numeric literals

String literals usually compile much faster than numeric literals, so it is advised to use string literals whenever possible (it is also the default).

Be aware that there are two known caveats when using string literals:

1. Microsoft Visual Studio versions prior to 2022 v17.0 [do not support string literals larger than 65535 bytes](https://learn.microsoft.com/en-us/cpp/cpp/string-and-character-literals-cpp?view=msvc-170#size-of-string-literals).
2. The data array will contain an extra byte: the null termination of the string (the `*_len` value is not affected, though). In the vast majority of cases this is inconsequential, as the extra trailing byte is simply ignored.

## How fast is it?

Let us compare lsb2s with a widely available binary-to-source tool, [xxd](https://linux.die.net/man/1/xxd), that can output C code with the `-include` option.

### Conversion speed

For large files, lsb2s can read about **2 GB/s** of raw binary data (and write 8 GB/s of source code), which is almost **200 times faster** than xxd, which reads about 11 MB/s:

![Conversion speed](media/conversion_speed_large_files.svg "Conversion speed")

For smaller file sizes, process startup overhead dominates, but lsb2s has a clear advantage as soon as the binary file size exceeds a couple of kilobytes:

![Conversion speed](media/conversion_speed_vs_file_size.svg "Conversion speed")

*Measurements were done on an AMD Ryzen 3900X (underclocked @ 3.5GHz), with the binary file on a [RAM disk](https://www.kernel.org/doc/html/latest/filesystems/tmpfs.html) and the generated source code redirected to `/dev/null` in order to minimize I/O overhead.*

## What's the trick?

The binary-to-source conversion routine uses a look-up-table to convert byte values (0-255) to packets of four ASCII characters at a time. On most CPU architectures this essentially boils down to three machine code instructions (or less) per raw byte.

For example (slightly simplified), on x86:

```asm
    movzbl (%rcx),%edi          // Load binary byte from the input buffer
    mov    (%r12,%rdi,4),%edi   // Look up ASCII respresentation
    mov    %edi,0x4(%rdx)       // Store four ASCII characters in the output buffer
```

This is *significantly* faster than using something like `printf("%d,", input[i])` for instance. The result is that in almost all situations the conversion is not the bottleneck, but rather the file I/O or process startup limits the speed. In other words, lsb2s often has similar performance as a regular file copy.
