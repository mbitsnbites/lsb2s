// -*- mode: c; tab-width: 2; indent-tabs-mode: nil; -*-
//-----------------------------------------------------------------------------
// lsb2s - A tool to convert binary data to program source code, at ludicrous
// speed.
//-----------------------------------------------------------------------------
// Copyright (c) 2024 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//-----------------------------------------------------------------------------

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LSB2S_VERSION "1.0.0"
#define LSB2S_COPYRIGHT_YEAR "2024"

//-----------------------------------------------------------------------------
// Detect architecture endianity.
//-----------------------------------------------------------------------------

#if (defined(__BYTE_ORDER) && __BYTE_ORDER == __BIG_ENDIAN) ||               \
    defined(__BIG_ENDIAN__) || defined(__ARMEB__) || defined(__THUMBEB__) || \
    defined(__AARCH64EB__) || defined(_MIBSEB) || defined(__MIBSEB) ||       \
    defined(__MIBSEB__)
#define LSB2S_BIG_ENDIAN 1
#elif (defined(__BYTE_ORDER) && __BYTE_ORDER == __LITTLE_ENDIAN) ||       \
    defined(__LITTLE_ENDIAN__) || defined(__ARMEL__) ||                   \
    defined(__THUMBEL__) || defined(__AARCH64EL__) || defined(_MIPSEL) || \
    defined(__MIPSEL) || defined(__MIPSEL__)
#define LSB2S_LITTLE_ENDIAN 1
#else
// Unable to determine the endianity of the architecture.
#warning "Using runtime LUT generation instead of compile time generation."
#define LSB2S_UNKNOWN_ENDIAN 1
#endif

//-----------------------------------------------------------------------------
// Convert four ASCII characters into a uint32_t, with correct endianity.
//-----------------------------------------------------------------------------

#if defined(LSB2S_BIG_ENDIAN)
// Big endian version.
#define ASCII2U32(a, b, c, d)                            \
  ((((uint32_t)(a)) << 24U) | (((uint32_t)(b)) << 16U) | \
   (((uint32_t)(c)) << 8U) | ((uint32_t)(d)))
#elif defined(LSB2S_LITTLE_ENDIAN)
// Little endian version.
#define ASCII2U32(a, b, c, d)                                             \
  (((uint32_t)(a)) | (((uint32_t)(b)) << 8U) | (((uint32_t)(c)) << 16U) | \
   (((uint32_t)(d)) << 24U))
#else
// Runtime version.
static inline uint32_t ASCII2U32(uint32_t a,
                                 uint32_t b,
                                 uint32_t c,
                                 uint32_t d) {
  const char buf[4] = {a, b, c, d};
  uint32_t x;
  memcpy(&x, &buf[0], 4);
  return x;
}
#endif

//-----------------------------------------------------------------------------
// We use look-up-tables to convert a integer values in the range 0-255 to
// four-byte ASCII sequences
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ITON: The four-byte sequence consists of the ASCII decimal representation of
// the value (padded to three characters) followed by a comma, as follows:
//
//    0  ->  "  0,"
//    1  ->  "  1,"
//       ...
//   23  ->  " 23,"
//   24  ->  " 24,"
//       ...
//  254  ->  "254,"
//  255  ->  "255,"
//-----------------------------------------------------------------------------

#define ITON_D1(x) (((x) % 10U) + 48U)
#define ITON_D10(x) ((((x) / 10U) % 10U) + 48U)
#define ITON_D100(x) ((((x) / 100U) % 10U) + 48U)
#define ITON(x)                               \
  ASCII2U32((x) >= 100U ? ITON_D100(x) : 32U, \
            (x) >= 10U ? ITON_D10(x) : 32U,   \
            ITON_D1(x),                       \
            44U)

//-----------------------------------------------------------------------------
// ITOS: The four-byte sequence consists of the escaped ASCII hex
// representation of the value, as follows:
//
//    0  ->  "\x00"
//    1  ->  "\x01"
//       ...
//   23  ->  "\x17"
//   24  ->  "\x18"
//       ...
//  254  ->  "\xfe"
//  255  ->  "\xff"
//-----------------------------------------------------------------------------

#define ITOS_HEX4(x) ((x) + ((x) <= 9U ? 48U : 87U))
#define ITOS(x) \
  ASCII2U32('\\', 'x', ITOS_HEX4(((x) >> 4U) & 15U), ITOS_HEX4((x)&15U))

#if !defined(LSB2S_UNKNOWN_ENDIAN)
// We define the LUTs at compile time, so init is a no-op.
#define init_iton_lut()
#define init_itos_lut()

// clang-format off
static const uint32_t ITON_LUT[256] = {
    ITON(0U),   ITON(1U),   ITON(2U),   ITON(3U),   ITON(4U),   ITON(5U),
    ITON(6U),   ITON(7U),   ITON(8U),   ITON(9U),   ITON(10U),  ITON(11U),
    ITON(12U),  ITON(13U),  ITON(14U),  ITON(15U),  ITON(16U),  ITON(17U),
    ITON(18U),  ITON(19U),  ITON(20U),  ITON(21U),  ITON(22U),  ITON(23U),
    ITON(24U),  ITON(25U),  ITON(26U),  ITON(27U),  ITON(28U),  ITON(29U),
    ITON(30U),  ITON(31U),  ITON(32U),  ITON(33U),  ITON(34U),  ITON(35U),
    ITON(36U),  ITON(37U),  ITON(38U),  ITON(39U),  ITON(40U),  ITON(41U),
    ITON(42U),  ITON(43U),  ITON(44U),  ITON(45U),  ITON(46U),  ITON(47U),
    ITON(48U),  ITON(49U),  ITON(50U),  ITON(51U),  ITON(52U),  ITON(53U),
    ITON(54U),  ITON(55U),  ITON(56U),  ITON(57U),  ITON(58U),  ITON(59U),
    ITON(60U),  ITON(61U),  ITON(62U),  ITON(63U),  ITON(64U),  ITON(65U),
    ITON(66U),  ITON(67U),  ITON(68U),  ITON(69U),  ITON(70U),  ITON(71U),
    ITON(72U),  ITON(73U),  ITON(74U),  ITON(75U),  ITON(76U),  ITON(77U),
    ITON(78U),  ITON(79U),  ITON(80U),  ITON(81U),  ITON(82U),  ITON(83U),
    ITON(84U),  ITON(85U),  ITON(86U),  ITON(87U),  ITON(88U),  ITON(89U),
    ITON(90U),  ITON(91U),  ITON(92U),  ITON(93U),  ITON(94U),  ITON(95U),
    ITON(96U),  ITON(97U),  ITON(98U),  ITON(99U),  ITON(100U), ITON(101U),
    ITON(102U), ITON(103U), ITON(104U), ITON(105U), ITON(106U), ITON(107U),
    ITON(108U), ITON(109U), ITON(110U), ITON(111U), ITON(112U), ITON(113U),
    ITON(114U), ITON(115U), ITON(116U), ITON(117U), ITON(118U), ITON(119U),
    ITON(120U), ITON(121U), ITON(122U), ITON(123U), ITON(124U), ITON(125U),
    ITON(126U), ITON(127U), ITON(128U), ITON(129U), ITON(130U), ITON(131U),
    ITON(132U), ITON(133U), ITON(134U), ITON(135U), ITON(136U), ITON(137U),
    ITON(138U), ITON(139U), ITON(140U), ITON(141U), ITON(142U), ITON(143U),
    ITON(144U), ITON(145U), ITON(146U), ITON(147U), ITON(148U), ITON(149U),
    ITON(150U), ITON(151U), ITON(152U), ITON(153U), ITON(154U), ITON(155U),
    ITON(156U), ITON(157U), ITON(158U), ITON(159U), ITON(160U), ITON(161U),
    ITON(162U), ITON(163U), ITON(164U), ITON(165U), ITON(166U), ITON(167U),
    ITON(168U), ITON(169U), ITON(170U), ITON(171U), ITON(172U), ITON(173U),
    ITON(174U), ITON(175U), ITON(176U), ITON(177U), ITON(178U), ITON(179U),
    ITON(180U), ITON(181U), ITON(182U), ITON(183U), ITON(184U), ITON(185U),
    ITON(186U), ITON(187U), ITON(188U), ITON(189U), ITON(190U), ITON(191U),
    ITON(192U), ITON(193U), ITON(194U), ITON(195U), ITON(196U), ITON(197U),
    ITON(198U), ITON(199U), ITON(200U), ITON(201U), ITON(202U), ITON(203U),
    ITON(204U), ITON(205U), ITON(206U), ITON(207U), ITON(208U), ITON(209U),
    ITON(210U), ITON(211U), ITON(212U), ITON(213U), ITON(214U), ITON(215U),
    ITON(216U), ITON(217U), ITON(218U), ITON(219U), ITON(220U), ITON(221U),
    ITON(222U), ITON(223U), ITON(224U), ITON(225U), ITON(226U), ITON(227U),
    ITON(228U), ITON(229U), ITON(230U), ITON(231U), ITON(232U), ITON(233U),
    ITON(234U), ITON(235U), ITON(236U), ITON(237U), ITON(238U), ITON(239U),
    ITON(240U), ITON(241U), ITON(242U), ITON(243U), ITON(244U), ITON(245U),
    ITON(246U), ITON(247U), ITON(248U), ITON(249U), ITON(250U), ITON(251U),
    ITON(252U), ITON(253U), ITON(254U), ITON(255U)
};

static const uint32_t ITOS_LUT[256] = {
    ITOS(0U),   ITOS(1U),   ITOS(2U),   ITOS(3U),   ITOS(4U),   ITOS(5U),
    ITOS(6U),   ITOS(7U),   ITOS(8U),   ITOS(9U),   ITOS(10U),  ITOS(11U),
    ITOS(12U),  ITOS(13U),  ITOS(14U),  ITOS(15U),  ITOS(16U),  ITOS(17U),
    ITOS(18U),  ITOS(19U),  ITOS(20U),  ITOS(21U),  ITOS(22U),  ITOS(23U),
    ITOS(24U),  ITOS(25U),  ITOS(26U),  ITOS(27U),  ITOS(28U),  ITOS(29U),
    ITOS(30U),  ITOS(31U),  ITOS(32U),  ITOS(33U),  ITOS(34U),  ITOS(35U),
    ITOS(36U),  ITOS(37U),  ITOS(38U),  ITOS(39U),  ITOS(40U),  ITOS(41U),
    ITOS(42U),  ITOS(43U),  ITOS(44U),  ITOS(45U),  ITOS(46U),  ITOS(47U),
    ITOS(48U),  ITOS(49U),  ITOS(50U),  ITOS(51U),  ITOS(52U),  ITOS(53U),
    ITOS(54U),  ITOS(55U),  ITOS(56U),  ITOS(57U),  ITOS(58U),  ITOS(59U),
    ITOS(60U),  ITOS(61U),  ITOS(62U),  ITOS(63U),  ITOS(64U),  ITOS(65U),
    ITOS(66U),  ITOS(67U),  ITOS(68U),  ITOS(69U),  ITOS(70U),  ITOS(71U),
    ITOS(72U),  ITOS(73U),  ITOS(74U),  ITOS(75U),  ITOS(76U),  ITOS(77U),
    ITOS(78U),  ITOS(79U),  ITOS(80U),  ITOS(81U),  ITOS(82U),  ITOS(83U),
    ITOS(84U),  ITOS(85U),  ITOS(86U),  ITOS(87U),  ITOS(88U),  ITOS(89U),
    ITOS(90U),  ITOS(91U),  ITOS(92U),  ITOS(93U),  ITOS(94U),  ITOS(95U),
    ITOS(96U),  ITOS(97U),  ITOS(98U),  ITOS(99U),  ITOS(100U), ITOS(101U),
    ITOS(102U), ITOS(103U), ITOS(104U), ITOS(105U), ITOS(106U), ITOS(107U),
    ITOS(108U), ITOS(109U), ITOS(110U), ITOS(111U), ITOS(112U), ITOS(113U),
    ITOS(114U), ITOS(115U), ITOS(116U), ITOS(117U), ITOS(118U), ITOS(119U),
    ITOS(120U), ITOS(121U), ITOS(122U), ITOS(123U), ITOS(124U), ITOS(125U),
    ITOS(126U), ITOS(127U), ITOS(128U), ITOS(129U), ITOS(130U), ITOS(131U),
    ITOS(132U), ITOS(133U), ITOS(134U), ITOS(135U), ITOS(136U), ITOS(137U),
    ITOS(138U), ITOS(139U), ITOS(140U), ITOS(141U), ITOS(142U), ITOS(143U),
    ITOS(144U), ITOS(145U), ITOS(146U), ITOS(147U), ITOS(148U), ITOS(149U),
    ITOS(150U), ITOS(151U), ITOS(152U), ITOS(153U), ITOS(154U), ITOS(155U),
    ITOS(156U), ITOS(157U), ITOS(158U), ITOS(159U), ITOS(160U), ITOS(161U),
    ITOS(162U), ITOS(163U), ITOS(164U), ITOS(165U), ITOS(166U), ITOS(167U),
    ITOS(168U), ITOS(169U), ITOS(170U), ITOS(171U), ITOS(172U), ITOS(173U),
    ITOS(174U), ITOS(175U), ITOS(176U), ITOS(177U), ITOS(178U), ITOS(179U),
    ITOS(180U), ITOS(181U), ITOS(182U), ITOS(183U), ITOS(184U), ITOS(185U),
    ITOS(186U), ITOS(187U), ITOS(188U), ITOS(189U), ITOS(190U), ITOS(191U),
    ITOS(192U), ITOS(193U), ITOS(194U), ITOS(195U), ITOS(196U), ITOS(197U),
    ITOS(198U), ITOS(199U), ITOS(200U), ITOS(201U), ITOS(202U), ITOS(203U),
    ITOS(204U), ITOS(205U), ITOS(206U), ITOS(207U), ITOS(208U), ITOS(209U),
    ITOS(210U), ITOS(211U), ITOS(212U), ITOS(213U), ITOS(214U), ITOS(215U),
    ITOS(216U), ITOS(217U), ITOS(218U), ITOS(219U), ITOS(220U), ITOS(221U),
    ITOS(222U), ITOS(223U), ITOS(224U), ITOS(225U), ITOS(226U), ITOS(227U),
    ITOS(228U), ITOS(229U), ITOS(230U), ITOS(231U), ITOS(232U), ITOS(233U),
    ITOS(234U), ITOS(235U), ITOS(236U), ITOS(237U), ITOS(238U), ITOS(239U),
    ITOS(240U), ITOS(241U), ITOS(242U), ITOS(243U), ITOS(244U), ITOS(245U),
    ITOS(246U), ITOS(247U), ITOS(248U), ITOS(249U), ITOS(250U), ITOS(251U),
    ITOS(252U), ITOS(253U), ITOS(254U), ITOS(255U)
};
// clang-format on
#else
static uint32_t ITON_LUT[256];
static uint32_t ITOS_LUT[256];

void init_iton_lut(void) {
  for (uint32_t i = 0U; i <= 255U; ++i) {
    ITON_LUT[i] = ITON(i);
  }
}

void init_itos_lut(void) {
  for (uint32_t i = 0U; i <= 255U; ++i) {
    ITOS_LUT[i] = ITOS(i);
  }
}
#endif

//-----------------------------------------------------------------------------
// Input stream configuration.
//-----------------------------------------------------------------------------

// Number of bytes to buffer from the binary file (must be a power of two).
#define BIN_STREAM_CAPACITY 65536

// Statically allocated buffer for the input data.
static uint8_t s_in_buf[BIN_STREAM_CAPACITY];

//-----------------------------------------------------------------------------
// Common output implementation for all languages.
//-----------------------------------------------------------------------------

// Output buffer configuration:
//  * Capacity is given in number of uint32_t:s.
//  * There's one uint32_t per binary byte, plus one per row (newline etc).
#define SRC_NUM_PER_ROW 32  // Must be a power of two.
#define SRC_BUF_CAPACITY                                                     \
  (((BIN_STREAM_CAPACITY * (SRC_NUM_PER_ROW + 1)) + (SRC_NUM_PER_ROW - 1)) / \
   SRC_NUM_PER_ROW)

// Statically allocated buffer for the output data.
static uint32_t s_out_buf[SRC_BUF_CAPACITY];

static size_t convert(FILE* in_file,
                      FILE* out_file,
                      const uint32_t row_start,
                      const uint32_t* restrict const lut) {
  size_t total_size = 0U;

  while (true) {
    // Read more data (until we reach EOF).
    const size_t bytes_read =
        fread(&s_in_buf[0], 1, BIN_STREAM_CAPACITY, in_file);
    if (bytes_read == 0U) {
      break;
    }
    total_size += bytes_read;

    uint8_t* restrict in_buf_ptr = &s_in_buf[0];
    uint32_t* restrict out_buf_ptr = &s_out_buf[0];

    // Append all full rows of C code to the buffer.
    size_t bytes_left = bytes_read;
    for (; bytes_left >= SRC_NUM_PER_ROW; bytes_left -= SRC_NUM_PER_ROW) {
      *out_buf_ptr++ = row_start;
      for (int i = 0; i < SRC_NUM_PER_ROW; i += 4) {
        const uint8_t b1 = in_buf_ptr[0];
        const uint8_t b2 = in_buf_ptr[1];
        const uint8_t b3 = in_buf_ptr[2];
        const uint8_t b4 = in_buf_ptr[3];
        const uint32_t ascii1 = lut[b1];
        const uint32_t ascii2 = lut[b2];
        const uint32_t ascii3 = lut[b3];
        const uint32_t ascii4 = lut[b4];
        out_buf_ptr[0] = ascii1;
        out_buf_ptr[1] = ascii2;
        out_buf_ptr[2] = ascii3;
        out_buf_ptr[3] = ascii4;
        in_buf_ptr += 4;
        out_buf_ptr += 4;
      }
    }

    // If there is a partial row left, append it now.
    if (bytes_left > 0U) {
      *out_buf_ptr++ = row_start;
      for (int i = 0; i < (int)bytes_left; ++i) {
        *out_buf_ptr++ = lut[*in_buf_ptr++];
      }
    }

    // Write the buffer to the output file.
    const size_t out_buf_size = out_buf_ptr - &s_out_buf[0];
    fwrite(&s_out_buf[0], 1, sizeof(uint32_t) * out_buf_size, out_file);
  }

  return total_size;
}

//-----------------------------------------------------------------------------
// C language output.
//-----------------------------------------------------------------------------

static void convert_to_c(FILE* in_file,
                         FILE* out_file,
                         const char* var_name,
                         bool numeric) {
  // Configurable literal format (default to string literals).
  uint32_t row_start = ASCII2U32('"', '\n', ' ', '"');
  const uint32_t* lut = ITOS_LUT;
  const char* array_start = "\"";
  const char* array_end = "\"";
  if (numeric) {
    row_start = ASCII2U32('\n', ' ', ' ', ' ');
    lut = ITON_LUT;
    array_start = "{";
    array_end = "}";
  }

  // Print file header.
  fprintf(out_file, "const unsigned char %s[] = %s", var_name, array_start);

  // Convert the data.
  const size_t total_size = convert(in_file, out_file, row_start, lut);

  // Print the footer (with the size, now that we know it).
  fprintf(out_file,
          "%s;\nconst unsigned int %s_len = %" PRIu64 "U;\n",
          array_end,
          var_name,
          (uint64_t)total_size);
}

//-----------------------------------------------------------------------------
// Go language output.
//-----------------------------------------------------------------------------

static void convert_to_go(FILE* in_file,
                          FILE* out_file,
                          const char* var_name,
                          const char* package_name,
                          bool numeric) {
  uint32_t row_start = ASCII2U32('"', '\n', ' ', '"');
  const uint32_t* lut = ITOS_LUT;
  const char* array_end = "\"";
  if (numeric) {
    row_start = ASCII2U32('\n', ' ', ' ', ' ');
    lut = ITON_LUT;
    array_end = "}";
  }

  // Print file header.
  fprintf(out_file, "package %s\n", package_name);
  if (numeric) {
    fprintf(out_file, "var %s = []byte{", var_name);
  } else {
    fprintf(out_file, "const %s = \"", var_name);
  }

  // Convert the data.
  const size_t total_size = convert(in_file, out_file, row_start, lut);

  // Print the footer (with the size, now that we know it).
  fprintf(out_file,
          "%s\nconst %s_len = %" PRIu64 "\n",
          array_end,
          var_name,
          (uint64_t)total_size);
}

//-----------------------------------------------------------------------------
// JavaScript language output.
//-----------------------------------------------------------------------------

static void convert_to_js(FILE* in_file,
                          FILE* out_file,
                          const char* var_name) {
  const uint32_t row_start = ASCII2U32('\n', ' ', ' ', ' ');

  // Print file header.
  fprintf(out_file, "const %s = Uint8Array.from([", var_name);

  // Convert the data.
  (void)convert(in_file, out_file, row_start, ITON_LUT);

  // Print the footer.
  fprintf(out_file, "])\n");
}

//-----------------------------------------------------------------------------
// Supported output languages.
//-----------------------------------------------------------------------------

typedef enum lang_e {
  LANG_C = 0,
  LANG_GO = 1,
  LANG_JS = 2,
} lang_t;

static bool parse_lang_str(const char* lang_str, lang_t* lang) {
  if (strcmp(lang_str, "c") == 0) {
    *lang = LANG_C;
    return true;
  }
  if (strcmp(lang_str, "go") == 0) {
    *lang = LANG_GO;
    return true;
  }
  if (strcmp(lang_str, "js") == 0 || strcmp(lang_str, "javascript") == 0) {
    *lang = LANG_JS;
    return true;
  }
  return false;
}

//-----------------------------------------------------------------------------
// CLI front-end.
//-----------------------------------------------------------------------------

static void print_usage(const char* prg_name) {
  fprintf(stderr, "Usage: %s [OPTIONS] [BINFILE]\n", prg_name);
  fprintf(stderr, "\nOPTIONS\n");
  fprintf(stderr, "  -h, --help     display help and exit\n");
  fprintf(stderr, "  -l LANG, --language=LANG\n");
  fprintf(stderr, "                 emit code for language LANG\n");
  fprintf(stderr, "  -n NAME, --name=NAME\n");
  fprintf(stderr, "                 define variable name (default: data)\n");
  fprintf(stderr,
          "  -N, --numeric  use numeric literals instead of string literals\n");
  fprintf(stderr, "  -o OUTFILE, --output=OUTFILE\n");
  fprintf(stderr, "                 write to OUTFILE (default: stdout)\n");
  fprintf(stderr, "  -p PACKAGE, --package=PACKAGE\n");
  fprintf(stderr, "                 package name (for Go, default: main)\n");
  fprintf(stderr, "  -V, --version  display program version and exit\n");
  fprintf(stderr, "\nLANG\n");
  fprintf(stderr, "  c              C (default)\n");
  fprintf(stderr, "  go             Go\n");
  fprintf(stderr, "  js             JavaScript\n");
}

static void print_version(void) {
  printf("lsb2b v" LSB2S_VERSION
         " - Ludicrous speed binary to source converter\n");
  printf("Copyright (c) " LSB2S_COPYRIGHT_YEAR " Marcus Geelnard\n");
  printf("Released under the open source zlib/libpng license.\n");
  printf("https://opensource.org/license/zlib\n");
}

static bool starts_with(const char* s, const char* start) {
  const size_t len = strlen(start);
  return strncmp(s, start, len) == 0;
}

int main(int argc, char** argv) {
  // Parse arguments.
  const char* lang_str = "c";
  const char* var_name = "data";
  bool numeric = false;
  const char* src_filename = NULL;
  const char* package_name = "main";
  const char* bin_filename = NULL;
  for (int k = 1; k < argc; ++k) {
    const char* arg = argv[k];
    if (arg[0] == '-') {
      if (strcmp(arg, "-h") == 0 || strcmp(arg, "--help") == 0) {
        print_usage(argv[0]);
        exit(0);
      } else if (strcmp(arg, "-l") == 0) {
        if (k >= (argc - 1)) {
          fprintf(stderr, "%s: Missing argument after %s.\n", argv[0], arg);
          print_usage(argv[0]);
          exit(1);
        }
        lang_str = argv[k + 1];
        ++k;
      } else if (starts_with(arg, "--language=")) {
        lang_str = &argv[k][11];
      } else if (strcmp(arg, "-n") == 0) {
        if (k >= (argc - 1)) {
          fprintf(stderr, "%s: Missing argument after %s.\n", argv[0], arg);
          print_usage(argv[0]);
          exit(1);
        }
        var_name = argv[k + 1];
        ++k;
      } else if (starts_with(arg, "--name=")) {
        var_name = &argv[k][7];
      } else if (strcmp(arg, "-N") == 0 || strcmp(arg, "--numeric") == 0) {
        numeric = true;
      } else if (strcmp(arg, "-o") == 0) {
        if (k >= (argc - 1)) {
          fprintf(stderr, "%s: Missing argument after %s.\n", argv[0], arg);
          print_usage(argv[0]);
          exit(1);
        }
        src_filename = argv[k + 1];
        ++k;
      } else if (starts_with(arg, "--output=")) {
        src_filename = &argv[k][9];
      } else if (strcmp(arg, "-p") == 0) {
        if (k >= (argc - 1)) {
          fprintf(stderr, "%s: Missing argument after %s.\n", argv[0], arg);
          print_usage(argv[0]);
          exit(1);
        }
        package_name = argv[k + 1];
        ++k;
      } else if (starts_with(arg, "--package=")) {
        package_name = &argv[k][10];
      } else if (strcmp(arg, "-V") == 0 || strcmp(arg, "--version") == 0) {
        print_version();
        exit(0);
      } else {
        fprintf(stderr, "%s: Unknown option: %s\n", argv[0], arg);
        print_usage(argv[0]);
        exit(1);
      }
    } else {
      if (bin_filename == NULL) {
        bin_filename = arg;
      } else {
        fprintf(stderr, "%s: Too many arguments: %s\n", argv[0], arg);
        print_usage(argv[0]);
        exit(1);
      }
    }
  }
  lang_t lang;
  if (!parse_lang_str(lang_str, &lang)) {
    fprintf(stderr, "%s: Invalid language selection: %s\n", argv[0], lang_str);
    exit(1);
  }

  // Prepare LUTs (this is a no-op if the LUTs are generated att compile time).
  if (numeric) {
    init_iton_lut();
  } else {
    init_itos_lut();
  }

  // Open the input file.
  FILE* bin_file;
  if (bin_filename != NULL) {
    bin_file = fopen(bin_filename, "rb");
    if (bin_file == NULL) {
      fprintf(stderr, "Unable to open binary file %s\n", bin_filename);
      return false;
    }
  } else {
    // Read data from stdin.
    bin_file = stdin;
  }

  // Open the output file.
  FILE* src_file;
  if (src_filename != NULL) {
    src_file = fopen(src_filename, "wb");
    if (src_file == NULL) {
      fprintf(stderr, "Unable to open source file %s\n", src_filename);
      return false;
    }
  } else {
    // Use stdout as the output.
    src_file = stdout;
  }

  // Do the actual conversion.
  switch (lang) {
    default:
    case LANG_C:
      convert_to_c(bin_file, src_file, var_name, numeric);
      break;
    case LANG_GO:
      convert_to_go(bin_file, src_file, var_name, package_name, numeric);
      break;
    case LANG_JS:
      convert_to_js(bin_file, src_file, var_name);
      break;
  }

  // Close the output file.
  if (src_filename != NULL) {
    fclose(src_file);
  }

  // Close the input file.
  if (bin_filename != NULL) {
    fclose(bin_file);
  }

  return 0;
}
